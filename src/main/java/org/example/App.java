package org.example;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

public class App
{
    public static void main( String[] args )
    {
        try (InputStream input = new FileInputStream("target\\maven-archiver\\pom.properties")) {

            Properties prop = new Properties();

            prop.load(input);

            Enumeration<?> enumeration = prop.propertyNames();

            while (enumeration.hasMoreElements()) {
                String key = enumeration.nextElement().toString();
                System.out.println(key + " : " + prop.getProperty(key));
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}

